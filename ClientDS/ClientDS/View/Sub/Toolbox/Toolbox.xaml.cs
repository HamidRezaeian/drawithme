﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientDS.View.Sub.Toolbox
{
    /// <summary>
    /// Interaction logic for Toolbox.xaml
    /// </summary>
    public partial class Toolbox : UserControl
    {
        public Toolbox()
        {
            InitializeComponent();
            Model.StaticData.color = Colors.Black;
            buttonColor.Click += ButtonColor_Click;
            buttonEraser.Click += ButtonEraser_Click;
            buttonMarker.Click += ButtonMarker_Click;
            buttonNastaliq.Click += ButtonNastaliq_Click;
            buttonPencil.Click += ButtonPencil_Click;
            buttonCurve.Click += ButtonCurve_Click;
        }

        private void ButtonCurve_Click(object sender, RoutedEventArgs e)
        {
            if (((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.FitToCurve==false)
                ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.FitToCurve = true;
            else
                ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.FitToCurve = false;
        }
        private void ButtonPencil_Click(object sender, RoutedEventArgs e)
        {
            //pencil
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Color = Model.StaticData.color;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.IsHighlighter = false;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Height = 5;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Width = 5;
        }

        private void ButtonNastaliq_Click(object sender, RoutedEventArgs e)
        {
            //nastaligh
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Color = Model.StaticData.color;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.IsHighlighter = false;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Height = 20;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Width = 1;
        }

        private void ButtonMarker_Click(object sender, RoutedEventArgs e)
        {
            //mazhik
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Color = Model.StaticData.color;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.IsHighlighter = true;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Height = 20;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Width = 20;
        }

        private void ButtonEraser_Click(object sender, RoutedEventArgs e)
        {

            //eraser
            Model.StaticData.color = ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Color;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.IsHighlighter = false;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Color = Colors.White;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Height = 50;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Width = 50;
        }

        private void ButtonColor_Click(object sender, RoutedEventArgs e)
        {
            Model.ToolBox.Toolbox toolbox = new Model.ToolBox.Toolbox();
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Color = toolbox.colorPicker();
            Model.StaticData.color = ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Color;
        }
    }
}
