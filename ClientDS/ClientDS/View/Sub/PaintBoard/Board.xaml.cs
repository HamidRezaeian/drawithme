﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClientDS.Model.MiddleWare;


namespace ClientDS.View.Sub.PaintBoard
{
    /// <summary>
    /// Interaction logic for Board.xaml
    /// </summary>
    public partial class Board : UserControl
    {
        public Board()
        {
            InitializeComponent();
            ViewModel.BoardVM.board = this;
            ((View.Sub.PaintBoard.Board)ViewModel.BoardVM.board).PaintBoard.MouseLeftButtonUp += PaintBoard_MouseLeftButtonUp;

            ((View.Sub.PaintBoard.Board)ViewModel.BoardVM.board).layer2.Background = new ImageBrush();
        }

        string err = "";
        private static void Background_Changed(object sender, EventArgs e)
        {
            ((View.Sub.PaintBoard.Board)ViewModel.BoardVM.board).PaintBoard.Strokes.Clear();
        }
        private void PaintBoard_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try {
                if (!Model.MiddleWare.Winsock.SendData(Model.MiddleWare.Converter.Canvas2PNGArray(((View.Sub.PaintBoard.Board)ViewModel.BoardVM.board).PaintBoard), ref err))
                    MessageBox.Show(err);
            }
            catch(Exception ew)
            {
                MessageBox.Show(ew.Message);
            }
            
        }

    }
}
