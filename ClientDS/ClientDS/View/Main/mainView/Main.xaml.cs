﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ClientDS.View.Main.mainView
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : UserControl
    {
        string err = "";
        DispatcherTimer timer = new DispatcherTimer();
        public Main()
        {
            InitializeComponent();
            ViewModel.MainVM.Main = this;
            this.Loaded += Main_Loaded;
        }

        private void Main_Loaded(object sender, RoutedEventArgs e)
        {
            //pencil
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Color = Model.StaticData.color;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.IsHighlighter = false;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Height = 5;
            ((View.Sub.PaintBoard.Board)(ViewModel.BoardVM.board)).PaintBoard.DefaultDrawingAttributes.Width = 5;
            Model.MiddleWare.Winsock.SendData("First", ref err);
        }

        private void GridLoading_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (((View.Main.mainView.Main)ViewModel.MainVM.Main).GridLoading.Visibility == Visibility.Visible)
            {
                Model.MiddleWare.Winsock.connect(ref err,true);
                
                timer.IsEnabled = true;
                timer.Start();
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (Model.MiddleWare.Winsock.sockSend.State==7)
            {
                timer.IsEnabled = false;
                ((View.Main.mainView.Main)ViewModel.MainVM.Main).GridLoading.Visibility = Visibility.Collapsed;
            }
            else
            {
                if (Model.MiddleWare.Winsock.sockSend.State != 6)
                {
                    Model.MiddleWare.Winsock.connect(ref err, true);
                }
            }
        }
    }
}
