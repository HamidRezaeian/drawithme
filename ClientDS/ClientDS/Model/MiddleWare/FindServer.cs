﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace ClientDS.Model.MiddleWare
{
    public static class FindServer
    {
        private static DispatcherTimer[] timer = new DispatcherTimer[256];
        private static MSWinsockLib.Winsock[] sock = new MSWinsockLib.Winsock[256];
        public static void find()
        {
            string ip = localip();
            for (int i = 0; i < 255; i++)
            {
                timer[i] = new DispatcherTimer();
                timer[i].Tag = i;
                sock[i] = new MSWinsockLib.Winsock();
                connect(ip + i.ToString(), sock[i],timer[i]);
            }
        }
        private static string localip()
        {
            MSWinsockLib.Winsock tempwinsock = new MSWinsockLib.Winsock();
            string[] s = tempwinsock.LocalIP.Split('.');
            string S = "";

            for (int i = 0; i < 3; i++)
            {
                S = S + s[i]+".";
            }
            return S;

        }

        private static bool connect(string ip, MSWinsockLib.Winsock sockSend, DispatcherTimer timer, int port = 55000)
        {

            try
            {

                sockSend.Connect(ip, port);
                timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
                timer.IsEnabled = true;
                timer.Tick += Timer_Tick;
                timer.Start();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        private static void Timer_Tick(object sender, EventArgs e)
        {
            
            int i = Convert.ToInt32(((DispatcherTimer)sender).Tag);
            if (sock[i].State ==7)
            {
                if (sock[i].RemoteHostIP != StaticData.ServerIP)
                {
                    StaticData.ServerIP = sock[i].RemoteHostIP;
                    sock[i].SendComplete += () =>
                    {
                        sock[i].Close();
                    };
                    sock[i].SendData(Winsock.Data);
                }
                ViewModel.WindowVM.window.Title ="Local IP:"+sock[i].LocalIP+" | Server IP:"+ StaticData.ServerIP;
                for (int i1 = 0; i1 < 255; i1++)
                {
                    timer[i1].IsEnabled = false;
                }
                    
            }
        }


    }
}
