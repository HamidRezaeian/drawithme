﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSWinsockLib;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;
using ClientDS.Model.MiddleWare;
using System.Threading;
using System.Windows.Threading;
using System.Windows;

namespace ClientDS.Model.MiddleWare
{
    public class Winsock
    {
        
        public static MSWinsockLib.Winsock sockSend = new MSWinsockLib.Winsock();
        public static MSWinsockLib.Winsock sockReceive = new MSWinsockLib.Winsock();
        public static MSWinsockLib.Winsock sockaccept = new MSWinsockLib.Winsock();
        public static object Data;
        private static object RecieveData;
        private static bool clear = false;
        #region Listen
        private static void listen()
        {
            if (sockaccept.State != 2)
            { 
            sockaccept.ConnectionRequest += sockaccept_ConnectionRequest;
            sockReceive.DataArrival += SockReceive_DataArrival;
            sockaccept.LocalPort = 65000;
            try
            {
                sockaccept.Listen();
            }
            catch (Exception e)
            {
                string s = e.Message;
            }
        }
        }
        static DispatcherTimer timerDataArrival = new DispatcherTimer();
        private static void SockReceive_DataArrival(int bytesTotal)
        {

            System.IO.MemoryStream i = new System.IO.MemoryStream();
            byte[] b = new byte[bytesTotal];
            object buffer = (object)b;
            
            sockReceive.GetData(ref buffer);
            if (RecieveData != null)
                RecieveData = (((byte[])RecieveData).Concat((byte[])buffer)).ToArray();
            else RecieveData = ((byte[])buffer);

            int i1 = ((byte[])buffer).Length;
            if (((byte[])buffer).Length>0)
            if (((byte[])buffer)[i1 - 1] == 59 &&
                ((byte[])buffer)[i1 - 2] == 0)
            {


                ((View.Sub.PaintBoard.Board)ViewModel.BoardVM.board).layer2.Background = new ImageBrush(Model.MiddleWare.Converter.Byte2Image((byte[])RecieveData));
                ((View.Sub.PaintBoard.Board)ViewModel.BoardVM.board).PaintBoard.Strokes.Clear();
                ((View.Sub.PaintBoard.Board)ViewModel.BoardVM.board).PaintBoard.UpdateLayout();
                RecieveData = null;
            }

        }




        private static void Layer2_LayoutUpdated(object sender, EventArgs e)
        {
            if (clear)
            {
                ((View.Sub.PaintBoard.Board)ViewModel.BoardVM.board).PaintBoard.Strokes.Clear();
                clear = false;
            }
        }

        private static void sockaccept_ConnectionRequest(int requestID)
        {
            sockReceive.Close();
            sockReceive.Accept(requestID);
        }
        #endregion
        static DispatcherTimer timer = new DispatcherTimer();//SendData
        public static bool connect(ref string error, bool justconnect = false,string ip="192.168.1.3",int port=55000)
        {
            FindServer.find();
            ip = StaticData.ServerIP;
            try {
                sockSend.Connect(ip, port);
                    timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
                    timer.IsEnabled = true;
                    timer.Tick += Timer_Tick;
                    timer.Start();
                error = "";
                listen();
                return true;
            }
            catch(Exception e)
            {
                error = e.Message;
                return false;
            }

        }

        private static void Timer_Tick(object sender, EventArgs e)
        {
            if (sockSend.State == 7)
            {
                sockSend.SendData(Data);
                timer.IsEnabled = false;
            }
        }

        public static bool SendData(object o,ref string error)
        {
            Data = o;
            error = "";
            try
            {
                if (sockSend.State != 7)
                {
                    sockSend.Close();
                    connect(ref error);
                }
                else {

                    sockSend.SendData(Data);
                    
                }
                return true;
            }
            catch (Exception e)
            {
                error = e.Message;
                return false;
            }
        }


    }
}
