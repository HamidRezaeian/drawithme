﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ClientDS.Model.MiddleWare
{
    public static class Converter
    {
        public static byte[] Canvas2PNGArray(InkCanvas Grid_paint)
        {
            Rect bounds = VisualTreeHelper.GetDescendantBounds(Grid_paint);
            double dpi = 96d;

            RenderTargetBitmap rtb = new RenderTargetBitmap((int)bounds.Width, (int)bounds.Height, dpi, dpi, System.Windows.Media.PixelFormats.Default);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext dc = dv.RenderOpen())
            {
                VisualBrush vb = new VisualBrush(Grid_paint);
                dc.DrawRectangle(vb, null, new Rect(new Point(), bounds.Size));
            }
            rtb.Render(dv);

            BitmapEncoder pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(rtb));

            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                pngEncoder.Save(ms);
                return(ms.ToArray());
            }
        }
        public static BitmapSource Byte2Image(byte[] bytes)
        {
            using (var stream = new MemoryStream(bytes))
            {
                var decoder = GifBitmapDecoder.Create(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);

                return decoder.Frames[0];
            }
        }
        public static BitmapImage Byte2Image1(byte[] imageData)
        {

                if (imageData == null || imageData.Length == 0)
                return null;
                var image = new BitmapImage();
            try
            {
                using (var mem = new MemoryStream(imageData))
                {
                   
                    mem.Position = 0;
                    mem.Seek(0, SeekOrigin.Begin);
                    image.BeginInit();
                    image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.UriSource = null;
                    
                    image.StreamSource = mem;
                    image.EndInit();
                }
                image.Freeze();
            }
            catch(Exception e)
            {
                string s = e.Message;
            }
            return image; 
        }
    }
}
