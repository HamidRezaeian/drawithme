# Draw With Me!

![logo](markdown/drawithme.png)

Let's paint together! This is my project on Distributed Systems course at pasargad university. I did this project individually in three weeks. Also this project was selected as the best project by the course instructor. This software requires windows OS because of utilizing .Net framework. But the ability to implement the client on all platforms is available and through the defined port, it is able to communicate with the server.

below figure depicts the communication method between clients with server.

![schema](markdown/schema2.png)
- as you can see the server listen port is 55000 and client listen port is 65000. 
- send/receive data as byte array.
- The backup file is updated on the server side after each change.

![schema](markdown/schema1.png)
### How does it work?
as you can see in the above figure, We have two layers on UI.

1- at first a user start drawing on layer1 on client side.

2- then the first layer convert into PNG transparent format and send as byte array to the server.

3- in this step data received on the server side on layer1.

4- now we merge layer1 and layer2.

5- And we convert it to the GIF format.

6- then after backup we send it to the client/clients as byte array.

7- in this step client receives the data of layer2.

8- remove layer1 screen on client side.

Note: step 8 help to reduces the size of data sent later on and thus increases the transparency of the system. 

# Transparency
Transparency is an important characteristic of distributed systems and at all stages of the design of this system it has been considered.

- The user does not know if there are multiple copies of the backup file, or where the backup file is.
- The user does not notice that the first layer of the drawing is cleared.
- The user does not know when the server is online or offline.
- If the server software is closed during operation, the user will not notice it.
- If the server IP or even the client IP changes during operation, the user will not notice it.
- Communications in this system are asynchronous. Although, in some cases like establishing a connection, the need for synchronization is felt and i have used multi-threading to keep the transparency and non-stop system in place.

##### Note: This system is optimized for the local network by default and you should run it on the local network for maximum transparency.

## Requirement:
- [x] MSWinsockLib
- [x] .NET Framework
 