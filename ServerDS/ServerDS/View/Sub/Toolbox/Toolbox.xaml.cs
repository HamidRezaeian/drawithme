﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ServerDS.View.Sub.Toolbox
{
    /// <summary>
    /// Interaction logic for Toolbox.xaml
    /// </summary>
    public partial class Toolbox : UserControl
    {

        public Toolbox()
        {
            InitializeComponent();
            toggleButtonMultithread.Checked += ToggleButtonMultithread_Checked;
            toggleButtonMultithread.Unchecked += ToggleButtonMultithread_Unchecked;
        }

        private void ToggleButtonMultithread_Unchecked(object sender, RoutedEventArgs e)
        {
            Model.MiddleWare.Winsock.MultiThread = false;
        }

        private void ToggleButtonMultithread_Checked(object sender, RoutedEventArgs e)
        {
            Model.MiddleWare.Winsock.MultiThread = true;
        }
    }
}
