﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ServerDS.View.Sub.Capture
{
    /// <summary>
    /// Interaction logic for Capture.xaml
    /// </summary>
    public partial class Capture : UserControl
    {
        public Capture()
        {
            InitializeComponent();
            ViewModel.CaptureVM.Layer2 = this.Layer2;
            ViewModel.CaptureVM.CaptureBoard = this.CaptureBoard;
            try {
                ViewModel.CaptureVM.Layer2.Background = new ImageBrush(Model.MiddleWare.Converter.BitmapFromUri(Model.StaticData.StaticData.BackupUri));
            }
            catch { }

           

        }
    }
}
