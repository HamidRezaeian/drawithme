﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ServerDS.View.Main.mainView
{
    /// <summary>
    /// Interaction logic for main.xaml
    /// </summary>
    public partial class main : UserControl
    {
        public main()
        {
            InitializeComponent();
            this.Loaded += Main_Loaded;
        }

        private void Main_Loaded(object sender, RoutedEventArgs e)
        {
            Model.MiddleWare.Winsock.listen();
        }
    }
}
