﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace ServerDS.Model.MiddleWare
{
    public static class Winsock
    {
        private static MSWinsockLib.Winsock[] sockSend = new MSWinsockLib.Winsock[256];
        private static MSWinsockLib.Winsock[] sockReceive= new MSWinsockLib.Winsock[10000];
        private static MSWinsockLib.Winsock sockaccept = new MSWinsockLib.Winsock();
        private static object Data;
        private static object[] RecieveData = new object[10000];
        private static int socknum=0;
        private static bool first = true;
        public static bool MultiThread = false;
        public static int receiverCounter = 0;

        #region Listen
        public static void listen(int Localport= 55000)
        {
            
            sockaccept.ConnectionRequest += sockaccept_ConnectionRequest;
            sockaccept.LocalPort = Localport;
            sockaccept.Listen();
        }
        static DispatcherTimer timerDataArrival = new DispatcherTimer();
        private static void SockReceive_DataArrival(int bytesTotal,int id)
        {
            string error = "";
            first = true;

            byte[] buffersamplefirst = { 70, 105, 114, 115, 116 };
            byte[] b = new byte[bytesTotal];
            object buffer = (object)b;
            sockReceive[id].GetData(ref buffer);

            if (RecieveData[id] != null)
                RecieveData[id] = (((byte[])RecieveData[id]).Concat((byte[])buffer)).ToArray();
            else RecieveData[id] = (byte[])buffer;

            for (int i = 0; i < 5; i++)
            {
                if (((Byte[])buffer)[i] != buffersamplefirst[i])
                {
                    first = false;
                    break;
                }
            }
            if (!first)
            {
                int i1 = ((byte[])buffer).Length;
                if (((byte[])buffer)[i1 - 1] == 130 &&
                    ((byte[])buffer)[i1 - 2] == 96 &&
                    ((byte[])buffer)[i1 - 3] == 66 &&
                    ((byte[])buffer)[i1 - 4] == 174 &&
                    ((byte[])buffer)[i1 - 5] == 68 &&
                    ((byte[])buffer)[i1 - 6] == 78 &&
                    ((byte[])buffer)[i1 - 7] == 69 &&
                    ((byte[])buffer)[i1 - 8] == 73)
                {
                    ViewModel.CaptureVM.CaptureBoard.Background = new ImageBrush(Converter.Byte2Image((byte[])RecieveData[id]));
                    ViewModel.CaptureVM.CaptureBoard.UpdateLayout();
                    Model.SystemIO.SaveBackup();
                    try
                    {
                        SendData2all(Converter.Layer2GifArray(ViewModel.CaptureVM.Layer2), ref error);
                    }
                    catch
                    { }
                    RecieveData[id] = null;
                }


            }
            else
            {
                try
                {
                    SendData2all(Converter.Layer2GifArray(ViewModel.CaptureVM.Layer2), ref error);
                }
                catch
                { }
                RecieveData[id] = null;
            }
        }
       
        private static void sockaccept_ConnectionRequest(int requestID)
        {
            try
            {
                sockReceive[receiverCounter] = new MSWinsockLib.Winsock();
                sockReceive[receiverCounter].Close();
                sockReceive[receiverCounter].Accept(requestID);
                StaticData.StaticData.AddClient(sockReceive[receiverCounter].RemoteHostIP);
            }
            catch (Exception e)
            {
                MessageBox.Show("Error On Accept: " + e.Message);
            }

            try
            {
                Thread t = new Thread(new ParameterizedThreadStart(accept_innewthread));
                t.Name =Convert.ToString(receiverCounter);
                t.Start(requestID);
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        private static void accept_innewthread(object id)
        {
            string s = Thread.CurrentThread.Name;
            sockReceive[receiverCounter].DataArrival += (int bytetotal) =>
              {
                  SockReceive_DataArrival(bytetotal,Convert.ToInt32(s));
              };
            receiverCounter++;
        }
        #endregion
        static DispatcherTimer[] timer=new DispatcherTimer[256];//SendData
        private static bool connect(ref string error, string ip = "127.0.0.1", int port = 65000)
        {
            again:
            try
            {
                timer[socknum].Tag = socknum;
                timer[socknum].Interval = new TimeSpan(0, 0,0,0, 1);
                timer[socknum].IsEnabled = true;
                sockSend[socknum].Connect(ip, port);
                timer[socknum].Tick += Timer_Tick;
                timer[socknum].Start();

                
                error = "";
                return true;
            }
            catch (Exception e)
            {
                error = e.Message;
                if (error == "Object reference not set to an instance of an object.")
                {
                    timer[socknum] = new DispatcherTimer();
                    goto again;
                }
                return false;
            }

        }
        private static void Timer_Tick(object sender, EventArgs e)
        {
            int i = Convert.ToInt32(((DispatcherTimer)sender).Tag);
            if (sockSend[i].State == 7)
            {
                sockSend[i].SendData(Data);
                timer[i].IsEnabled = false;
            }
        }
        public static void SendData2all(object o, ref string error)
        {
                socknum = 0;
                if (MultiThread)
                {
                    foreach (var client in StaticData.StaticData.ClientsIP)
                    {
                        SendData(o, client, ref error);
                        socknum++;
                    }
                }
                else {
                    SendData(o, StaticData.StaticData.ClientsIP[0], ref error);
                }
        }
        
        public static void newSendthread(object _socknum)
        {
            sockSend[(int)_socknum].SendData(Data);
        }
        public static bool SendData(object o,string ip, ref string error)
        {
            Data = o;
            again1:

            error = "";
            try
            {
                if (sockSend[socknum].State != 7)
                {
                    sockSend[socknum].SendProgress += Winsock_SendProgress;
                    sockSend[socknum].Close();
                    connect(ref error,ip);
                }
                else {
                    if (MultiThread)
                    {
                        
                        Thread thread = new Thread(new ParameterizedThreadStart(newSendthread));
                        thread.Start(socknum);
                    }
                    else
                    {
                        sockSend[socknum].SendData(o);
                    }



                }
                return true;
            }
            catch (Exception e)
            {
                error = e.Message;
                if (error == "Object reference not set to an instance of an object.")
                {
                    sockSend[socknum] = new MSWinsockLib.Winsock();
                    goto again1;
                }
                return false;
            }
            
        }

        private static void Winsock_SendProgress(int bytesSent, int bytesRemaining)
        {
            if (!MultiThread)
            {
                string error = "";
                socknum++;
                if (StaticData.StaticData.ClientsIP.Count > socknum)
                    SendData(Data, StaticData.StaticData.ClientsIP[socknum], ref error);
            }
        }
    }
}
