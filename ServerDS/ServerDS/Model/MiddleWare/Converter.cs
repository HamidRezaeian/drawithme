﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Interop;
namespace ServerDS.Model.MiddleWare
{
    public static class Converter
    {

        public static byte[] Layer2GifArray(Grid Grid_paint)
        {
            Rect bounds = VisualTreeHelper.GetDescendantBounds(Grid_paint);
            double dpi = 96d;

            RenderTargetBitmap rtb = new RenderTargetBitmap((int)bounds.Width, (int)bounds.Height, dpi, dpi, System.Windows.Media.PixelFormats.Default);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext dc = dv.RenderOpen())
            {
                VisualBrush vb = new VisualBrush(Grid_paint);
                dc.DrawRectangle(vb, null, new Rect(new System.Windows.Point(), bounds.Size));
            }
            rtb.Render(dv);

            BitmapEncoder GifEncoder = new GifBitmapEncoder();
            GifEncoder.Frames.Add(BitmapFrame.Create(rtb));

            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                GifEncoder.Save(ms);
                return (ms.ToArray());
            }
        }
        public static ImageSource BitmapFromUri(string source)
        {
            var bitmap = new BitmapImage();
            var stream = File.OpenRead(source);
            bitmap.BeginInit();
            bitmap.CacheOption = BitmapCacheOption.OnLoad;
            bitmap.StreamSource = stream;
            bitmap.EndInit();
            stream.Close();
            stream.Dispose();
            bitmap.Freeze();
            return bitmap;
        }
        public static BitmapSource Byte2Image(byte[] bytes)
        {
            using (var stream = new MemoryStream(bytes))
            {
                var decoder = GifBitmapDecoder.Create(stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
                return decoder.Frames[0];
            }
        }

        public static void SnapShotPNG(this UIElement source, Uri destination, int zoom)
        {
            destination = new Uri(Model.StaticData.StaticData.BackupUri);
            source = ViewModel.CaptureVM.Layer2;
            zoom = 1;
            try
            {
                double actualHeight = source.RenderSize.Height;
                double actualWidth = source.RenderSize.Width;

                double renderHeight = actualHeight * zoom;
                double renderWidth = actualWidth * zoom;

                RenderTargetBitmap renderTarget = new RenderTargetBitmap((int)renderWidth, (int)renderHeight, 96, 96, PixelFormats.Pbgra32);
                VisualBrush sourceBrush = new VisualBrush(source);

                DrawingVisual drawingVisual = new DrawingVisual();
                DrawingContext drawingContext = drawingVisual.RenderOpen();

                using (drawingContext)
                {
                    drawingContext.PushTransform(new ScaleTransform(zoom, zoom));
                    drawingContext.DrawRectangle(sourceBrush, null, new Rect(new System.Windows.Point(0, 0), new System.Windows.Point(actualWidth, actualHeight)));
                }
                renderTarget.Render(drawingVisual);

                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(renderTarget));
                using (FileStream stream = new FileStream(destination.LocalPath, FileMode.Create, FileAccess.Write))
                {
                    encoder.Save(stream);
                }
            }
            catch
            {

            }
        }

    }
}
