﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ServerDS.Model.StaticData
{
    public static class StaticData
    {
        public static string BackupUri =AppDomain.CurrentDomain.BaseDirectory+ @"Backup.gif";
        public static List<string> ClientsIP = new List<string>();
        public static void AddClient(string client)
        {
            ClientsIP.Add(client);
            ClientsIP = ClientsIP.Distinct().ToList();
            clientboxUpdate();
        }
        public static void RemoveClient(string client)
        {
            ClientsIP.Remove(client);
            clientboxUpdate();
        }
        private static void clientboxUpdate()
        {
            ((View.Sub.Clients.Clients)(ViewModel.ClientBoxVM.clientbox)).ClientsBox.Children.Clear();
            foreach (var Client in ClientsIP)
            {
                Label lable = new Label();
                lable.Content = Client+" "+Model.MiddleWare.Winsock.receiverCounter.ToString();
                lable.Foreground = System.Windows.Media.Brushes.White;
                ((View.Sub.Clients.Clients)(ViewModel.ClientBoxVM.clientbox)).ClientsBox.Children.Add(lable);
            }
        }
    }
}
