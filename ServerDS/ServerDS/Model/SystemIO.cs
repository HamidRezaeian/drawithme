﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ServerDS.Model
{
    public static class SystemIO
    {
        public static void SaveBackup()
        {
            System.IO.File.WriteAllBytes(Model.StaticData.StaticData.BackupUri,Model.MiddleWare.Converter.Layer2GifArray(ViewModel.CaptureVM.Layer2));
            LoadBackup();
        }
        public static void LoadBackup()
        {
                ViewModel.CaptureVM.Layer2.Background = new ImageBrush(Model.MiddleWare.Converter.BitmapFromUri(Model.StaticData.StaticData.BackupUri));
                //ViewModel.CaptureVM.CaptureBoard.Background = Brushes.Transparent;
        }

    }
}
